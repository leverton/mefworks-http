<?php namespace mef\Http\Test\Unit;

use Iterator;
use mef\Http\ArrayUtil;

/**
 * @coversDefaultClass \mef\Http\ArrayUtil
 */
class ArrayUtilTest extends \PHPUnit\Framework\TestCase
{
	/**
	 * @covers ::insertKeyAtLeaf
	 */
	public function testInsertKeyAtLeaf()
	{
		$input = [
			'foo' => 42,
			'bar' => [
				'a', 'b', 'c',
				'd' => 'e'
			]
		];

		$output = ArrayUtil::insertKeyAtLeaf($input, 'x');

		$this->assertTrue(is_array($output));

		$this->assertSame($output, [
			'foo' => ['x' => 42],
			'bar' => [
				['x' => 'a'],
				['x' => 'b'],
				['x' => 'c'],
				'd' => ['x' => 'e']
			]
		]);
	}

	/**
	 * @covers ::getRecursiveIterator
	 */
	public function testGetRecursiveIterator()
	{
		$input = [
			'a',
			1,
			null,
			false,
			3.14,
			(object) ['foo' => 'bar'],
			'children' => [
				'children' => [
					0, 1, 2, 3, 4
				],
				(object) ['a' => 1, 'b' => 2]
			]
		];

		$iterator = ArrayUtil::getRecursiveIterator($input);

		$this->assertTrue($iterator instanceof Iterator);

		$output = iterator_to_array($iterator);

		$this->assertSame(12, count($output));

		$types = [
			'string' => 0,
			'integer' => 0,
			'double' => 0,
			'boolean' => 0,
			'object' => 0,
			'NULL' => 0
		];

		foreach ($output as $value)
		{
			$types[gettype($value)]++;
		}

		$this->assertSame($types, [
  			'string' => 1,
  			'integer' => 6,
  			'double' => 1,
  			'boolean' => 1,
  			'object' => 2,
  			'NULL' => 1,
		]);
	}
}