<?php namespace mef\Http\Test\Unit;

use mef\Http\StringStream;

/**
 * @coversDefaultClass \mef\Http\StringStream
 */
class StringStreamTest extends AbstractStreamTest
{
	public function setup() : void
	{
		$this->stream = new StringStream('');
	}

	/**
	 * @covers ::__construct
	 */
	public function testConstructor()
	{
		$this->assertTrue($this->stream instanceof StringStream);
	}

	/**
	 * Writing in the middle of a StringStream truncates everything
	 * following that point.
	 *
	 * @covers ::write
	 * @covers ::isAttached
	 */
	public function testWriteInMiddle()
	{
		$this->stream->write('Hello, World!');
		$this->stream->seek(7, SEEK_SET);
		$this->stream->write('Jim!');

		$this->assertSame('Hello, Jim!', (string) $this->stream);
	}
}