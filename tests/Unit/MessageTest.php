<?php namespace mef\Http\Test\Unit;

use InvalidArgumentException;
use StdClass;
use Psr\Http\Message\StreamInterface;
use mef\Http\Message;

/**
 * @coversDefaultClass \mef\Http\Message
 */
class MessageTest extends \PHPUnit\Framework\TestCase
{
	/**
	 * @covers ::__construct
	 * @covers ::getBody
	 * @covers ::getHeaders
	 * @covers ::getProtocolVersion
	 */
	public function testConstructor()
	{
		$stream = $this->getMockBuilder(StreamInterface::class)->getMock();
		$headers = [
			'foo' => ['bar']
		];
		$protocol = '2.0';

		$message = new Message([
			'body' => $stream,
			'headers' => $headers,
			'protocolVersion' => $protocol
		]);

		$this->assertSame($stream, $message->getBody());
		$this->assertSame($headers, $message->getHeaders());
		$this->assertSame($protocol, $message->getProtocolVersion());
	}

	/**
	 * @covers ::__construct
	 * @covers ::getHeaderLine
	 */
	public function testConstructorWithStringHeader()
	{
		$stream = $this->getMockBuilder(StreamInterface::class)->getMock();
		$headers = [
			'foo' => 'bar'
		];
		$message = new Message([
			'body' => $stream,
			'headers' => $headers
		]);

		$this->assertSame('bar', $message->getHeaderLine('foo'));
	}

	/**
	 * @covers ::getProtocolVersion
	 * @covers ::withProtocolVersion
	 */
	public function testWithProtocolVersion()
	{
		$message = new Message(['body' => $this->getMockBuilder(StreamInterface::class)->getMock()]);
		$version = '2.0';

		$message2 = $message->withProtocolVersion($version);
		$this->assertTrue($message2 instanceof Message);
		$this->assertNotSame($message, $message2);
		$this->assertSame($version, $message2->getProtocolVersion());
	}

	/**
	 * @covers ::getProtocolVersion
	 * @covers ::withProtocolVersion
	 */
	public function testWithSameProtocolVersion()
	{
		$version = '2.0';
		$message = new Message([
			'body' => $this->getMockBuilder(StreamInterface::class)->getMock(),
			'protocolVersion' => $version
		]);

		$message2 = $message->withProtocolVersion($version);
		$this->assertTrue($message2 instanceof Message);
		$this->assertSame($message, $message2);
		$this->assertSame($version, $message2->getProtocolVersion());
	}

	/**
	 * @covers ::hasHeader
	 */
	public function testHasHeader()
	{
		$headers = [
			'foo' => ['bar']
		];
		$message = new Message([
			'body' => $this->getMockBuilder(StreamInterface::class)->getMock(),
			'headers' => $headers
		]);

		$this->assertFalse($message->hasHeader('bogus'));
		$this->assertTrue($message->hasHeader('foo'));
		$this->assertTrue($message->hasHeader('Foo'));
	}

	/**
	 * @covers ::getHeader
	 */
	public function testGetHeader()
	{
		$headers = [
			'foo' => ['bar', 'zed']
		];
		$message = new Message([
			'body' => $this->getMockBuilder(StreamInterface::class)->getMock(),
			'headers' => $headers
		]);

		$this->assertSame($headers['foo'], $message->getHeader('foo'));
		$this->assertSame($headers['foo'], $message->getHeader('Foo'));
		$this->assertSame([], $message->getHeader('bogus'));
	}

	/**
	 * @covers ::getHeaderLine
	 */
	public function testGetHeaderLine()
	{
		$headers = [
			'foo' => ['bar', 'zed']
		];
		$message = new Message([
			'body' => $this->getMockBuilder(StreamInterface::class)->getMock(),
			'headers' => $headers
		]);

		$this->assertSame('bar,zed', $message->getHeaderLine('foo'));
		$this->assertSame('bar,zed', $message->getHeaderLine('Foo'));
		$this->assertSame('', $message->getHeaderLine('bogus'));
	}

	/**
	 * @covers ::withHeader
	 */
	public function testWithHeaderString()
	{
		$message = new Message([
			'body' => $this->getMockBuilder(StreamInterface::class)->getMock()
		]);
		$message2 = $message->withHeader('foo', 'bar');

		$this->assertTrue($message2 instanceof Message);
		$this->assertNotSame($message, $message2);
		$this->assertSame('bar', $message2->getHeaderLine('foo'));
	}

	/**
	 * @covers ::withHeader
	 */
	public function testWithHeaderArray()
	{
		$message = new Message([
			'body' => $this->getMockBuilder(StreamInterface::class)->getMock()
		]);
		$message2 = $message->withHeader('foo', ['bar']);

		$this->assertTrue($message2 instanceof Message);
		$this->assertNotSame($message, $message2);
		$this->assertSame('bar', $message2->getHeaderLine('foo'));
	}

	/**
	 * @covers ::withHeader
	 */
	public function testWithHeaderSameString()
	{
		$message = new Message([
			'body' => $this->getMockBuilder(StreamInterface::class)->getMock(),
			'headers' => ['foo' => 'bar']
		]);
		$message2 = $message->withHeader('foo', 'bar');

		$this->assertTrue($message2 instanceof Message);
		$this->assertSame($message, $message2);
		$this->assertSame('bar', $message2->getHeaderLine('foo'));
	}

	/**
	 * @covers ::withAddedHeader
	 */
	public function testWithAddedNewHeader()
	{
		$message = new Message([
			'body' => $this->getMockBuilder(StreamInterface::class)->getMock()
		]);
		$message2 = $message->withAddedHeader('foo', 'bar');

		$this->assertTrue($message2 instanceof Message);
		$this->assertNotSame($message, $message2);
		$this->assertSame('bar', $message2->getHeaderLine('foo'));
	}

	/**
	 * @covers ::withAddedHeader
	 */
	public function testWithAddedSameHeader()
	{
		$message = new Message([
			'body' => $this->getMockBuilder(StreamInterface::class)->getMock(),
			'headers' => ['Foo' => 'bar']
		]);
		$message2 = $message->withAddedHeader('foo', 'bar');

		$this->assertTrue($message2 instanceof Message);
		$this->assertNotSame($message, $message2);
		$this->assertSame('bar,bar', $message2->getHeaderLine('foo'));
	}

	/**
	 * @covers ::withoutHeader
	 */
	public function testWithoutMissingHeader()
	{
		$message = new Message([
			'body' => $this->getMockBuilder(StreamInterface::class)->getMock()
		]);
		$message2 = $message->withoutHeader('bogus');

		$this->assertTrue($message2 instanceof Message);
		$this->assertSame($message, $message2);
		$this->assertSame([], $message2->getHeaders());
	}

	/**
	 * @covers ::withoutHeader
	 */
	public function testWithoutExistingHeader()
	{
		$message = new Message([
			'body' => $this->getMockBuilder(StreamInterface::class)->getMock(),
			'headers' => ['Foo' => 'bar']
		]);
		$message2 = $message->withoutHeader('Foo');

		$this->assertTrue($message2 instanceof Message);
		$this->assertNotSame($message, $message2);
		$this->assertSame([], $message2->getHeaders());
	}

	/**
	 * @covers ::withoutHeader
	 */
	public function testWithoutExistingHeaderDifferentCase()
	{
		$message = new Message([
			'body' => $this->getMockBuilder(StreamInterface::class)->getMock(),
			'headers' => ['Foo' => 'bar']
		]);
		$message2 = $message->withoutHeader('foo');

		$this->assertTrue($message2 instanceof Message);
		$this->assertNotSame($message, $message2);
		$this->assertSame([], $message2->getHeaders());
	}

	/**
	 * @covers ::withBody
	 */
	public function testWithSameBody()
	{
		$body = $this->getMockBuilder(StreamInterface::class)->getMock();
		$message = new Message(['body' => $body]);
		$message2 = $message->withBody($body);

		$this->assertTrue($message2 instanceof Message);
		$this->assertSame($message, $message2);
		$this->assertSame($body, $message2->getBody());
	}

	/**
	 * @covers ::withBody
	 */
	public function testWithNewBody()
	{
		$body = $this->getMockBuilder(StreamInterface::class)->getMock();
		$message = new Message(['body' => $body]);
		$message2 = $message->withBody($this->getMockBuilder(StreamInterface::class)->getMock());

		$this->assertTrue($message2 instanceof Message);
		$this->assertNotSame($message, $message2);
		$this->assertNotSame($body, $message2->getBody());
	}

	/**
	 * @covers ::filterArray
	 */
	public function testFilterArray()
	{
		$message = new Message([
			'body' => 'Hello, World!',
			'headers' => ['Foo' => 'bar'],
			'protocolVersion' => '1.0'
		]);

		$this->assertTrue($message instanceof Message);
		$this->assertTrue($message->getBody() instanceof StreamInterface);
		$this->assertSame('Hello, World!', (string) $message->getBody());
		$this->assertSame(['Foo' => ['bar']], $message->getHeaders());
		$this->assertSame('1.0', $message->getProtocolVersion());
	}

	/**
	 * @covers ::filterArray
	 */
	public function testInvalidBodyInArray()
	{
		$this->expectException(InvalidArgumentException::class);
		new Message(['body' => new StdClass]);
	}


	/**
	 * @covers ::filterArray
	 */
	public function testInvalidHeadersInArray()
	{
		$this->expectException(InvalidArgumentException::class);
		new Message(['headers' => 42]);
	}

	/**
	 * @covers ::filterArray
	 */
	public function testInvalidProtocolVersionInArray()
	{
		$this->expectException(InvalidArgumentException::class);
		new Message(['protocolVersion' => 42]);
	}

	/**
	 * @covers ::withHeader
	 * @covers ::getHeaders
	 */
	public function testChangingCaseOfHeader()
	{
		$message = new Message([
			'headers' => ['Foo' => 'bar'],
		]);

		$headers = $message->getHeaders();
		$this->assertSame(['Foo' => ['bar']], $headers);

		$message = $message->withHeader('foo', 'bar');
		$headers = $message->getHeaders();
		$this->assertSame(['foo' => ['bar']], $headers);
	}
}