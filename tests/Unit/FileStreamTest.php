<?php namespace mef\Http\Test\Unit;

use mef\Http\Exception\NoAttachedStreamException;
use mef\Http\Exception\ReadException;
use mef\Http\Exception\WriteException;
use mef\Http\FileStream;
use RuntimeException;

/**
 * @coversDefaultClass \mef\Http\FileStream
 */
class FileStreamTest extends AbstractStreamTest
{
	protected $fp;
	private $tmp;

	public function setup() : void
	{
		$this->fp = fopen('php://memory', 'rw');
		$this->stream = new FileStream($this->fp);
	}

	public function tearDown() : void
	{
		if ($this->tmp)
		{
			unlink($this->tmp);
			$this->tmp = '';
		}
	}

	/**
	 * @covers ::__construct
	 */
	public function testConstructor()
	{
		$this->assertTrue($this->stream instanceof FileStream);
	}

	/**
	 * @covers ::detach
	 * @covers ::__toString
	 */
	public function testResourceStillExistsAfterDetach()
	{
		$this->stream->write('Hello, World!');
		$this->stream->detach();

		fseek($this->fp, 0, SEEK_SET);
		$this->assertSame('Hello, World!', fgets($this->fp, 1000));
	}

	/**
	 * @covers ::detach
	 */
	public function testResourceIsReturnedByDetach()
	{
		$this->stream->write('Hello, World!');
		$fp = $this->stream->detach();

		$this->assertTrue(is_resource($fp));

		fseek($fp, 0, SEEK_SET);
		$this->assertSame('Hello, World!', fgets($fp, 1000));
	}

	/**
	 * @covers ::getSize
	 */
	public function testGetSize()
	{
		$this->assertNull($this->stream->getSize());
	}

	/**
	 * @covers ::write
	 */
	public function testCloseUnderlyingStreamThenWrite()
	{
		$this->expectException(RuntimeException::class);
		$data = 'Hello, World!';
		fclose($this->fp);
		@$this->stream->write($data);
	}

	/**
	 * @covers ::close
	 */
	public function testCloseUnderlyingStreamThenClose()
	{
		$this->expectException(NoAttachedStreamException::class);
		fclose($this->fp);
		@$this->stream->close();
	}

	/**
	 * @covers ::tell
	 */
	public function testCloseUnderlyingStreamThenTell()
	{
		$this->expectException(NoAttachedStreamException::class);
		fclose($this->fp);
		@$this->stream->tell();
	}

	/**
	 * @covers ::eof
	 */
	public function testCloseUnderlyingStreamThenEof()
	{
		$this->expectException(NoAttachedStreamException::class);
		fclose($this->fp);
		@$this->stream->eof();
	}

	/**
	 * @covers ::read
	 */
	public function testReadToWriteOnlyStream()
	{
		$this->tmp = tempnam(sys_get_temp_dir(), 'FileStreamTest');
		$this->expectException(ReadException::class);
		$writeOnly = fopen($this->tmp, 'w');
		$writeStream = new FileStream($writeOnly);
		@$writeStream->read(10);
	}

	/**
	 * @covers ::getContents
	 */
	public function testGetContentsOnWriteOnlyStream()
	{
		$this->tmp = tempnam(sys_get_temp_dir(), 'FileStreamTest');
		$this->expectException(ReadException::class);
		$writeOnly = fopen($this->tmp, 'w');
		$writeStream = new FileStream($writeOnly);
		@$writeStream->getContents();
	}

	/**
	 * @covers ::write
	 */
	public function testWriteToReadOnlyStream()
	{
		$this->expectException(WriteException::class);
		$readOnly = fopen('php://memory', 'r');
		$readStream = new FileStream($readOnly);

		$readStream->write("Hello, World!");
	}

	/**
	 * @covers ::getMetadata
	 */
	public function testGetMetadata()
	{
		$this->assertSame('php://memory', $this->stream->getMetadata('uri'));
	}

	/**
	 * @covers ::getMetadata
	 */
	public function testCloseUnderlyingStreamThenGetMetadata()
	{
		fclose($this->fp);
		$this->assertSame([], $this->stream->getMetadata());
	}
}