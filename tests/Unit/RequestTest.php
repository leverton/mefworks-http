<?php namespace mef\Http\Test\Unit;

use mef\Http\Request;
use mef\Http\Uri;

/**
 * @coversDefaultClass \mef\Http\Request
 */
class RequestTest extends \PHPUnit\Framework\TestCase
{
	/**
	 * @covers ::__construct
	 * @covers ::filterArray
	 */
	public function testFromMinimalArray()
	{
		$request = new Request([
			'uri' => 'http://localhost/'
		]);

		$this->assertTrue($request instanceof Request);
	}

	/**
	 * @covers ::__construct
	 * @covers ::filterArray
	 * @covers ::getRequestTarget
	 * @covers ::getUri
	 * @covers ::getMethod
	 */
	public function testFromFullArray()
	{
		$request = new Request([
			'method' => 'OPTIONS',
			'uri' => 'http://localhost/',
			'body' => '',
			'headers' => ['Host' => 'localhost'],
			'requestTarget' => '*',
			'protocolVersion' => '1.0'
		]);

		$this->assertTrue($request instanceof Request);
		$this->assertSame('OPTIONS', $request->getMethod());
		$this->assertSame('http://localhost/', (string) $request->getUri());
		$this->assertSame('', (string) $request->getBody());
		$this->assertSame(['Host' => ['localhost']], $request->getHeaders());
		$this->assertSame('*', $request->getRequestTarget());
		$this->assertSame('1.0', $request->getProtocolVersion());
	}

	/**
	 * @covers ::getRequestTarget
	 */
	public function testGetEmptyRequestTarget()
	{
		$request = new Request(['uri' => 'http://localhost/']);

		$this->assertTrue($request instanceof Request);
		$this->assertSame('/', $request->getRequestTarget());
	}

	/**
	 * @covers ::getRequestTarget
	 */
	public function testGetEmptyRequestTargetWithNoPath()
	{
		$request = new Request(['uri' => 'http://localhost']);

		$this->assertTrue($request instanceof Request);
		$this->assertSame('/', $request->getRequestTarget());
	}

	/**
	 * @covers ::getRequestTarget
	 */
	public function testGetEmptyRequestTargetWithQuery()
	{
		$request = new Request(['uri' => 'http://localhost/?foo=bar']);

		$this->assertTrue($request instanceof Request);
		$this->assertSame('/?foo=bar', $request->getRequestTarget());
	}

	/**
	 * @covers ::withRequestTarget
	 * @covers ::getRequestTarget
	 */
	public function testNewRequestTarget()
	{
		$request = new Request(['uri' => 'http://localhost']);
		$request2 = $request->withRequestTarget('*');

		$this->assertTrue($request2 instanceof Request);
		$this->assertNotSame($request, $request2);
		$this->assertSame('*', $request2->getRequestTarget());
	}

	/**
	 * @covers ::withRequestTarget
	 * @covers ::getRequestTarget
	 */
	public function testSameRequestTarget()
	{
		$request = new Request(['uri' => 'http://localhost', 'requestTarget' => '*']);
		$request2 = $request->withRequestTarget('*');

		$this->assertTrue($request2 instanceof Request);
		$this->assertSame($request, $request2);
		$this->assertSame('*', $request2->getRequestTarget());
	}

	/**
	 * @covers ::withMethod
	 * @covers ::getMethod
	 */
	public function testNewMethod()
	{
		$request = new Request([
			'method' => 'GET',
			'uri' => 'http://localhost'
		]);

		$this->assertTrue($request instanceof Request);

		$request2 = $request->withMethod('POST');

		$this->assertTrue($request2 instanceof Request);
		$this->assertNotSame($request, $request2);
		$this->assertSame('POST', $request2->getMethod());
	}

	/**
	 * @covers ::withMethod
	 * @covers ::getMethod
	 */
	public function testSameMethod()
	{
		$request = new Request([
			'method' => 'GET',
			'uri' => 'http://localhost'
		]);

		$this->assertTrue($request instanceof Request);

		$request2 = $request->withMethod('GET');

		$this->assertTrue($request2 instanceof Request);
		$this->assertSame($request, $request2);
		$this->assertSame('GET', $request2->getMethod());
	}

	/**
	 * @covers ::withUri
	 * @covers ::getUri
	 */
	public function testWithNewUri()
	{
		$request = new Request([
			'uri' => 'http://localhost'
		]);

		$this->assertTrue($request instanceof Request);

		$request2 = $request->withUri(Uri::fromString('http://localhost:8000'));

		$this->assertTrue($request2 instanceof Request);
		$this->assertNotSame($request, $request2);
		$this->assertSame('http://localhost:8000', (string) $request2->getUri());
	}

	/**
	 * @covers ::withUri
	 * @covers ::getUri
	 */
	public function testWithSameUri()
	{
		$request = new Request([
			'uri' => 'http://localhost',
			'headers' => ['Host' => 'localhost']
		]);

		$this->assertTrue($request instanceof Request);

		$request2 = $request->withUri(Uri::fromString('http://localhost'));

		$this->assertTrue($request2 instanceof Request);
		$this->assertSame($request, $request2);
		$this->assertSame('http://localhost', (string) $request2->getUri());
	}


	/**
	 * Test that an explicitly defined Host header is not overridden when
	 * $preserveHost is true.
	 *
	 * @covers ::withUri
	 * @covers ::getUri
	 */
	public function testWithNewUriAndPreservedHost()
	{
		$request = new Request([
			'uri' => 'http://localhost',
			'headers' => ['Host' => 'localhost']
		]);

		$this->assertTrue($request instanceof Request);

		$request2 = $request->withUri(Uri::fromString('http://localhost2'), true);

		$this->assertTrue($request2 instanceof Request);
		$this->assertNotSame($request, $request2);
		$this->assertSame('http://localhost2', (string) $request2->getUri());
		$this->assertSame('localhost', $request2->getHeaderLine('Host'));
	}

	/**
	 * Test that an empty Host header is overridden even when $preserveHost
	 * is true.
	 *
	 * @covers ::withUri
	 * @covers ::getUri
	 */
	public function testWithNewUriAndPreservedEmptyHost()
	{
		$request = new Request([
			'uri' => 'http://localhost',
			'headers' => ['Host' => '']
		]);

		$this->assertTrue($request instanceof Request);

		$request2 = $request->withUri(Uri::fromString('http://localhost2'), true);

		$this->assertTrue($request2 instanceof Request);
		$this->assertNotSame($request, $request2);
		$this->assertSame('http://localhost2', (string) $request2->getUri());
		$this->assertSame('localhost2', $request2->getHeaderLine('Host'));
	}

	/**
	 * @covers ::withUri
	 * @covers ::getUri
	 */
	public function testWithSameUriAndDifferentHostWhenPreservedHostIsFalse()
	{
		$request = new Request([
			'uri' => 'http://localhost',
			'headers' => ['Host' => '']
		]);

		$this->assertTrue($request instanceof Request);

		$request2 = $request->withUri(Uri::fromString('http://localhost'));

		$this->assertTrue($request2 instanceof Request);
		$this->assertNotSame($request, $request2);
		$this->assertSame('http://localhost', (string) $request2->getUri());
		$this->assertSame('localhost', $request2->getHeaderLine('Host'));
	}

	/**
	 * @covers ::withUri
	 * @covers ::getHeaderLine
	 */
	public function testWithHostAfterNewUri()
	{
		$request = new Request([
			'uri' => 'http://localhost',
			'headers' => ['Host' => 'localhost'],
		]);

		$this->assertSame('localhost', $request->getHeaderLine('Host'));

		$request = $request->withUri(Uri::fromString('http://google.com'));
		$this->assertSame('google.com', $request->getHeaderLine('Host'));
	}
}