<?php

namespace mef\Http;

use Throwable;
use Psr\Http\Message\StreamInterface;
use mef\Http\Exception\NoAttachedStreamException;
use mef\Http\Exception\ReadException;
use mef\Http\Exception\SeekException;
use mef\Http\Exception\WriteException;

class FileStream implements StreamInterface
{
    public function __construct(private mixed $stream)
    {
    }

    /**
     * Reads all data from the stream into a string, from the beginning to end.
     *
     * This method MUST attempt to seek to the beginning of the stream before
     * reading data and read the stream until the end is reached.
     *
     * Warning: This could attempt to load a large amount of data into memory.
     *
     * This method MUST NOT raise an exception in order to conform with PHP's
     * string casting operations.
     *
     * @see http://php.net/manual/en/language.oop5.magic.php#object.tostring
     * @return string
     */
    public function __toString(): string
    {
        try {
            $this->seek(0);
            $contents = stream_get_contents($this->stream);
        } catch (Throwable $e) {
            $contents = '';
        }

        return $contents;
    }

    /**
     * Closes the stream and any underlying resources.
     *
     * @return void
     * @throws NoAttachedStreamException if underlying stream has been closed
     */
    public function close(): void
    {
        if ($this->stream === null) {
            throw new NoAttachedStreamException();
        }

        try {
            fclose($this->stream);
        } catch (Throwable $e) {
            throw new NoAttachedStreamException($e);
        }

        $this->stream = null;
    }

    /**
     * Separates any underlying resources from the stream.
     *
     * After the stream has been detached, the stream is in an unusable state.
     *
     * @return resource|null Underlying PHP stream, if any
     */
    public function detach(): mixed
    {
        $detachedStream = $this->stream;

        $this->stream = null;

        return $detachedStream;
    }

    /**
     * Get the size of the stream if known.
     *
     * @return int|null Returns the size in bytes if known, or null if unknown.
     */
    public function getSize(): ?int
    {
        return null;
    }

    /**
     * Returns the current position of the file read/write pointer
     *
     * @return int Position of the file pointer
     * @throws NoAttachedStreamException if underlying stream has been closed
     */
    public function tell(): int
    {
        if ($this->stream === null) {
            throw new NoAttachedStreamException();
        }

        try {
            return ftell($this->stream);
        } catch (Throwable $e) {
            throw new NoAttachedStreamException($e);
        }
    }

    /**
     * Returns true if the stream is at the end of the stream.
     *
     * @return bool
     * @throws NoAttachedStreamException if underlying stream has been closed
     */
    public function eof(): bool
    {
        try {
            return $this->stream === null || feof($this->stream);
        } catch (Throwable $e) {
            throw new NoAttachedStreamException($e);
        }
    }

    /**
     * Returns whether or not the stream is seekable.
     *
     * @return bool
     */
    public function isSeekable(): bool
    {
        return $this->getMetaData('seekable') === true;
    }

    /**
     * Seek to a position in the stream.
     *
     * @link http://www.php.net/manual/en/function.fseek.php
     * @param int $offset Stream offset
     * @param int $whence Specifies how the cursor position will be calculated
     *     based on the seek offset. Valid values are identical to the built-in
     *     PHP $whence values for `fseek()`.  SEEK_SET: Set position equal to
     *     offset bytes SEEK_CUR: Set position to current location plus offset
     *     SEEK_END: Set position to end-of-stream plus offset.
     *
     * @throws NoAttachedStreamException if underlying stream has been closed
     * @throws SeekException on seek failure
     */
    public function seek($offset, $whence = SEEK_SET)
    {
        if ($this->stream === null) {
            throw new NoAttachedStreamException();
        }

        try {
            if (fseek($this->stream, $offset, $whence) !== 0) {
                throw new SeekException();
            }
        } catch (Throwable $e) {
            throw new NoAttachedStreamException($e);
        }
    }

    /**
     * Seek to the beginning of the stream.
     *
     * If the stream is not seekable, this method will raise an exception;
     * otherwise, it will perform a seek(0).
     *
     * @see seek()
     * @link http://www.php.net/manual/en/function.fseek.php
     *
     * @throws NoAttachedStreamException if underlying stream has been closed
     * @throws SeekException on seek failure.
     */
    public function rewind(): void
    {
        $this->seek(0);
    }

    /**
     * Returns whether or not the stream is writable.
     *
     * @return bool
     */
    public function isWritable(): bool
    {
        $mode = $this->getMetadata('mode');

        return (strlen($mode) > 0 && $mode[0] !== 'r') || (strlen($mode) > 1 && $mode[1] === '+');
    }

    /**
     * Write data to the stream.
     *
     * @param string $string The string that is to be written.
     * @return int Returns the number of bytes written to the stream.
     * @throws NoAttachedStreamException if underlying stream has been closed
     * @throws WriteException if unable to write
     */
    public function write($string): int
    {
        try {
            $bytesWritten = fwrite($this->stream, $string);
        } catch (Throwable $e) {
            throw new NoAttachedStreamException($e);
        }

        if ($bytesWritten === false) {
            throw new WriteException();
        }

        return $bytesWritten;
    }

    /**
     * Returns whether or not the stream is readable.
     *
     * @return bool
     */
    public function isReadable(): bool
    {
        $mode = $this->getMetadata('mode');

        return (strlen($mode) > 0 && $mode[0] === 'r') || (strlen($mode) > 1 && $mode[1] === '+');
    }

    /**
     * Read data from the stream.
     *
     * @param int $length Read up to $length bytes from the object and return
     *     them. Fewer than $length bytes may be returned if underlying stream
     *     call returns fewer bytes.
     * @return string Returns the data read from the stream, or an empty string
     *     if no bytes are available.
     * @throws NoAttachedStreamException if underlying stream has been closed
     * @throws ReadException if unable to read
     */
    public function read($length): string
    {
        try {
            $data = fread($this->stream, $length);
        } catch (Throwable $e) {
            throw new NoAttachedStreamException($e);
        }

        if ($data === false) {
            throw new ReadException();
        }

        return $data;
    }

    /**
     * Returns the remaining contents in a string
     *
     * @return string
     * @throws NoAttachedStreamException if underlying stream has been closed
     * @throws ReadException if unable to read
     */
    public function getContents(): string
    {
        try {
            $contents = stream_get_contents($this->stream);
        } catch (Throwable $e) {
            throw new NoAttachedStreamException($e);
        }

        if ($contents === false || ($contents === '' && $this->isReadable() === false)) {
            throw new ReadException();
        }

        return $contents;
    }

    /**
     * Get stream metadata as an associative array or retrieve a specific key.
     *
     * The keys returned are identical to the keys returned from PHP's
     * stream_get_meta_data() function.
     *
     * @link http://php.net/manual/en/function.stream-get-meta-data.php
     * @param string $key Specific metadata to retrieve.
     * @return array|mixed|null Returns an associative array if no key is
     *     provided. Returns a specific key value if a key is provided and the
     *     value is found, or null if the key is not found.
     */
    public function getMetadata($key = null): mixed
    {
        $data = [];

        if ($this->stream !== null) {
            try {
                $data = stream_get_meta_data($this->stream);

                if (is_array($data) === false) {
                    $data = [];
                }
            } catch (Throwable $e) {
                $data = [];
            }
        }

        if ($key === null) {
            return $data;
        } elseif (isset($data[$key]) === true) {
            return $data[$key];
        } else {
            return null;
        }
    }
}
