<?php

namespace mef\Http\Exception;

use RuntimeException;
use Throwable;

class SeekException extends RuntimeException
{
    public const ERROR_CODE = 1003;

    public function __construct(?Throwable $previous = null)
    {
        parent::__construct("Unable to seek to position.", self::ERROR_CODE, $previous);
    }
}
