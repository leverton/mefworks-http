<?php

namespace mef\Http\Exception;

use RuntimeException;
use Throwable;

class ReadException extends RuntimeException
{
    public const ERROR_CODE = 1001;

    public function __construct(?Throwable $previous = null)
    {
        parent::__construct("Unable to read from stream.", self::ERROR_CODE, $previous);
    }
}
