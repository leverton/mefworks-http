<?php

namespace mef\Http\Exception;

use RuntimeException;
use Throwable;

class WriteException extends RuntimeException
{
    public const ERROR_CODE = 1002;

    public function __construct(?Throwable $previous = null)
    {
        parent::__construct("Unable to write to stream.", self::ERROR_CODE, $previous);
    }
}
