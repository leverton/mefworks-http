<?php

namespace mef\Http\Exception;

use RuntimeException;
use Throwable;

class IoException extends RuntimeException
{
    public const ERROR_CODE = 1004;

    public function __construct(string $message = 'I/O Exception', ?Throwable $previous = null)
    {
        parent::__construct($message, self::ERROR_CODE, $previous);
    }
}
