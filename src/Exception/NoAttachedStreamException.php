<?php

namespace mef\Http\Exception;

use RuntimeException;
use Throwable;

class NoAttachedStreamException extends RuntimeException
{
    public const ERROR_CODE = 1000;

    public function __construct(?Throwable $previous = null)
    {
        parent::__construct("There is no attached stream.", self::ERROR_CODE, $previous);
    }
}
