<?php

namespace mef\Http;

use IteratorAggregate;
use Traversable;
use UnexpectedValueException;

/**
 * A sane representation of the $_FILES data.
 *
 * The normal way to use this class is:
 *
 * $files = (new \mef\Http\PhpFiles($_FILES))->getUploadedFiles();
 * echo $files['myfile']->getClientFilename();
 *
 * It can also be used as an iterator if you don't need the keys:
 *
 * foreach (new \mef\Http\PhpFiles($_FILES) as $file)
 * {
 *   echo $file->getClientFilename(), "\n";
 * }
 */
class PhpFiles implements IteratorAggregate
{
    /**
     * A normalized tree of files represented by UploadedFile objects
     *
     * @var array
     */
    private array $uploadedFiles;

    /**
     * Constructor
     *
     * @param array $files  The $_FILES array, or something that looks like it
     */
    public function __construct(array $files)
    {
        $this->uploadedFiles = $this->parsePhpFiles($files);
    }

    /**
     * Return all of the uploaded files as a "tree".
     *
     * Each leaf is an UploadedFile object.
     *
     * @return array
     */
    public function getUploadedFiles(): array
    {
        return $this->uploadedFiles;
    }

    public function getIterator(): Traversable
    {
        return ArrayUtil::getRecursiveIterator($this->uploadedFiles);
    }

    /**
     * Parse the $_FILES array (or any array with its format) into a collection
     * of \mef\Http\UploadedFile objects.
     *
     * The result is a tree where every leaf contains a file. Note that the
     * $_FILES array has a very strange format when it contains multiple
     * dimensions. This function assumes all input is in that format.
     *
     * @param  array  $files any array in the $_FILES format
     *
     * @return array
     * @throws UnexpectedValueException
     */
    private function parsePhpFiles(array $files): array
    {
        foreach ($files as $key => $file) {
            if (!is_array($file)) {
                throw new UnexpectedValueException('Unexpected value. All files must be represented by an array.');
            } elseif (isset($file['name'], $file['tmp_name'], $file['type'], $file['error'], $file['size']) === true) {
                if (
                    is_string($file['name']) === true && is_string($file['tmp_name']) === true
                    && is_string($file['type']) === true && is_int($file['error']) === true
                    && is_int($file['size']) === true
                ) {
                    $files[$key] = UploadedFile::fromPhpFile($file);
                } elseif (
                    is_array($file['name']) === true && is_array($file['tmp_name']) === true
                    && is_array($file['type']) === true && is_array($file['error']) === true
                    && is_array($file['size']) === true
                ) {
                    $files[$key] = $this->parsePhpFiles(array_replace_recursive(
                        ArrayUtil::insertKeyAtLeaf($file['name'], 'name'),
                        ArrayUtil::insertKeyAtLeaf($file['tmp_name'], 'tmp_name'),
                        ArrayUtil::insertKeyAtLeaf($file['type'], 'type'),
                        ArrayUtil::insertKeyAtLeaf($file['error'], 'error'),
                        ArrayUtil::insertKeyAtLeaf($file['size'], 'size')
                    ));
                } else {
                    throw new UnexpectedValueException('Unexpected format for the file array. name (string), tmp_name (string), type (string), error (int), and size (int) are all required fields.');
                }
            } else {
                $files[$key] = $this->parsePhpFiles($file);
            }
        }

        return $files;
    }
}
