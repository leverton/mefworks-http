<?php

namespace mef\Http;

use mef\Http\Exception\NoAttachedStreamException;
use Psr\Http\Message\StreamInterface;
use UnexpectedValueException;

class StringStream implements StreamInterface
{
    private int $pos = 0;

    private bool $eof = false;

    public function __construct(private ?string $data)
    {
    }

    /**
     * Reads all data from the stream into a string, from the beginning to end.
     *
     * This method MUST attempt to seek to the beginning of the stream before
     * reading data and read the stream until the end is reached.
     *
     * Warning: This could attempt to load a large amount of data into memory.
     *
     * This method MUST NOT raise an exception in order to conform with PHP's
     * string casting operations.
     *
     * @see http://php.net/manual/en/language.oop5.magic.php#object.tostring
     * @return string
     */
    public function __toString(): string
    {
        if (!$this->isAttached()) {
            return '';
        } else {
            return $this->data;
        }
    }

    /**
     * Closes the stream and any underlying resources.
     *
     * @return void
     * @throws NoAttachedStreamException if stream is already closed.
     */
    public function close(): void
    {
        if (!$this->isAttached()) {
            throw new NoAttachedStreamException();
        }

        $this->data = null;
        $this->eof = true;
    }

    /**
     * Separates any underlying resources from the stream.
     *
     * After the stream has been detached, the stream is in an unusable state.
     *
     * @return resource|null Underlying PHP stream, if any
     */
    public function detach(): mixed
    {
        $this->data = null;

        return null;
    }

    /**
     * Get the size of the stream if known.
     *
     * @return int|null Returns the size in bytes if known, or null if unknown.
     */
    public function getSize(): ?int
    {
        if (!$this->isAttached()) {
            return null;
        } else {
            return strlen($this->data);
        }
    }

    /**
     * Returns the current position of the file read/write pointer
     *
     * @return int Position of the file pointer
     * @throws NoAttachedStreamException if stream is already closed.
     */
    public function tell(): int
    {
        if (!$this->isAttached()) {
            throw new NoAttachedStreamException();
        }

        return $this->pos;
    }

    /**
     * Returns true if the stream is at the end of the stream.
     *
     * @return bool
     */
    public function eof(): bool
    {
        return $this->eof;
    }

    /**
     * Returns whether or not the stream is seekable.
     *
     * @return bool
     */
    public function isSeekable(): bool
    {
        return $this->isAttached();
    }

    /**
     * Seek to a position in the stream.
     *
     * @link http://www.php.net/manual/en/function.fseek.php
     * @param int $offset Stream offset
     * @param int $whence Specifies how the cursor position will be calculated
     *     based on the seek offset. Valid values are identical to the built-in
     *     PHP $whence values for `fseek()`.  SEEK_SET: Set position equal to
     *     offset bytes SEEK_CUR: Set position to current location plus offset
     *     SEEK_END: Set position to end-of-stream plus offset.
     * @throws UnexpectedValueException if either parameter is invalid.
     * @throws NoAttachedStreamException if stream is already closed.
     */
    public function seek($offset, $whence = SEEK_SET): void
    {
        if (!$this->isAttached()) {
            throw new NoAttachedStreamException();
        }

        if ($whence === SEEK_SET) {
            $newPos = $offset;
        } elseif ($whence === SEEK_CUR) {
            $newPos = $this->pos + $offset;
        } elseif ($whence === SEEK_END) {
            $newPos = strlen($this->data) + $offset;
        } else {
            throw new UnexpectedValueException("Whence is invalid. Must be SEEK_SET, SEEK_CUR, or SEEK_END.");
        }

        if ($newPos < 0 || $newPos > strlen($this->data)) {
            throw new UnexpectedValueException('The seek position is invalid');
        }

        $this->pos = $newPos;
        $this->eof = false;
    }

    /**
     * Seek to the beginning of the stream.
     *
     * If the stream is not seekable, this method will raise an exception;
     * otherwise, it will perform a seek(0).
     *
     * @see seek()
     * @link http://www.php.net/manual/en/function.fseek.php
     */
    public function rewind(): void
    {
        $this->seek(0);
    }

    /**
     * Returns whether or not the stream is writable.
     *
     * @return bool
     */
    public function isWritable(): bool
    {
        return $this->isAttached();
    }

    /**
     * Write data to the stream.
     *
     * @param string $string The string that is to be written.
     * @return int Returns the number of bytes written to the stream.
     * @throws NoAttachedStreamException if stream is already closed.
     */
    public function write($string): int
    {
        $string = (string) $string;

        if (!$this->isAttached()) {
            throw new NoAttachedStreamException();
        }

        if ($this->pos === strlen($this->data)) {
            $this->data .= $string;
        } else {
            $this->data = substr($this->data, 0, $this->pos) . $string;
        }

        $this->eof = false;

        $this->pos = strlen($this->data);

        return strlen($string);
    }

    /**
     * Returns whether or not the stream is readable.
     *
     * @return bool
     */
    public function isReadable(): bool
    {
        return $this->isAttached();
    }

    /**
     * Read data from the stream.
     *
     * @param int $length Read up to $length bytes from the object and return
     *     them. Fewer than $length bytes may be returned if underlying stream
     *     call returns fewer bytes.
     * @return string Returns the data read from the stream, or an empty string
     *     if no bytes are available.
     * @throws NoAttachedStreamException if stream is already closed.
     */
    public function read($length): string
    {
        if (!$this->isAttached()) {
            throw new NoAttachedStreamException();
        }

        if ($this->pos + $length > strlen($this->data)) {
            $length = strlen($this->data) - $this->pos;
            $this->eof = true;
        }

        if ($length === 0) {
            return '';
        } else {
            $readData = substr($this->data, $this->pos, $length);
            $this->pos += $length;

            return $readData;
        }
    }

    /**
     * Returns the remaining contents in a string
     *
     * @return string
     */
    public function getContents(): string
    {
        return $this->read(strlen($this->data ?? '') - $this->pos);
    }

    /**
     * Get stream metadata as an associative array or retrieve a specific key.
     *
     * The keys returned are identical to the keys returned from PHP's
     * stream_get_meta_data() function.
     *
     * @link http://php.net/manual/en/function.stream-get-meta-data.php
     * @param string $key Specific metadata to retrieve.
     * @return array|mixed|null Returns an associative array if no key is
     *     provided. Returns a specific key value if a key is provided and the
     *     value is found, or null if the key is not found.
     */
    public function getMetadata($key = null): mixed
    {
        if ($key === null) {
            return [];
        } else {
            return null;
        }
    }

    /**
     * @return bool True if there currently is a string attached.
     */
    private function isAttached(): bool
    {
        return $this->data !== null;
    }
}
