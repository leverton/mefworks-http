<?php namespace mef\Http\Example;

require_once __DIR__ . '/../vendor/autoload.php';

use mef\Http\Uri;

$uri = Uri::fromString('https://bitbucket.org/leverton/mefworks-http');

// A URI can be created from a string and treated as one.
// The individual components can be retrieved as defined by PSR-7.

echo "The example URI is {$uri}", PHP_EOL;
echo "* scheme: {$uri->getScheme()}", PHP_EOL;
echo "* host: {$uri->getHost()}", PHP_EOL;
echo "* path: {$uri->getPath()}", PHP_EOL;

// The URI is immutable. The various ->with() methods return a copy of a new
// object.

$uri = $uri->withScheme('http');

echo "After changing scheme to http, the URI is {$uri}", PHP_EOL;

// If specifying a default port, it will return null and be omitted from the
// string representation.

echo "The port is:", PHP_EOL;
var_dump($uri->getPort());

echo "After explicitly setting the port to 80 it is:", PHP_EOL;
$uri = $uri->withPort(80);
var_dump($uri->getPort());

// But since we explicitly set it, if we later change the scheme, we will be
// able to get it back.

$uri = $uri->withScheme('unknown');
echo "But after changing to an unknown scheme, it is: {$uri->getPort()}", PHP_EOL;

// It is possible to create a clone via fromString($uri), but it's better to
// just use the clone operator.

$uri = Uri::fromString($uri);
echo "The URI after calling ::fromString on itself is {$uri}", PHP_EOL;

$uri = clone $uri;
echo "The URI after calling clone() on itself is {$uri}", PHP_EOL;